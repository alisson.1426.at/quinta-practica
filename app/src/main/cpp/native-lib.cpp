#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_solorzanomedina_facci_practica5_14b_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
